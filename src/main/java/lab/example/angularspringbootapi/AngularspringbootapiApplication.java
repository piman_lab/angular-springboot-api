package lab.example.angularspringbootapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularspringbootapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularspringbootapiApplication.class, args);
	}

}
