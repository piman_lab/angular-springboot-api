package lab.example.angularspringbootapi.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * User
 */
@Entity
@JsonPropertyOrder({ "id", "first_name", "last_name", "age", "email" })
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Long id;

    @NotNull
    @Size(min = 2, max = 50, message = "Please provide first size between 2 - 100")
    @JsonProperty("first_name")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 100, message = "Please provide lastName size between 2 -100")
    @JsonProperty("last_name")
    private String lastName;

    @NotNull
    @Min(value = 18, message = "Please provide age >18")
    @JsonProperty("age")
    private Integer age;

    @Email(message = "Please provide valid email address")
    @JsonProperty("email")
    private String email;

    public Employee() {
    }

    public Employee(Long id,
            @NotNull @Size(min = 2, max = 50, message = "Please provide first size between 2 - 100") String firstName,
            @NotNull @Size(min = 2, max = 100, message = "Please provide lastName size between 2 - 100") String lastName,
            @NotNull @Min(value = 18, message = "Please provide age >18") Integer age,
            @Email(message = "Please provide valid email address") String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}