package lab.example.angularspringbootapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import lab.example.angularspringbootapi.entities.Employee;
import lab.example.angularspringbootapi.repositories.EmployeeRepository;

/**
 * EmployeeService
 */
@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository repository) {
        this.employeeRepository = repository;
    }

    public List<Employee> retrieveEmployee() {
        return (List<Employee>) employeeRepository.findAll();
    }

    public Optional<Employee> retrieveEmployee(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> retrieveEmployee(String name) {
        return employeeRepository.findByFirstName(name);
    }

    public Employee createEmployee(Employee customer) {
        return employeeRepository.save(customer);
    }

    public Optional<Employee> updateEmployee(Long id, Employee customer) {
        Optional<Employee> customerOptional = employeeRepository.findById(id);
        if (!customerOptional.isPresent()) {
            return customerOptional;
        }

        return Optional.of(employeeRepository.save(customer));
    }

    public boolean deleteEmployee(Long id) {
        try {
            employeeRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

}